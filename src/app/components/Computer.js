import React, {PropTypes, Component} from 'react';
import Dropzone from 'react-dropzone';

class Computer extends Component {

  handleOnDrop (a,b) {
    console.log(a,b);
  }

  render() {
    return (
      <div className="hola">
        <h2>Hola</h2>
        <Dropzone onDrop={this.handleOnDrop}>
          <p>Arrastra aquí tus archivos.</p>
        </Dropzone>
      </div>
    );
  }
}

Computer.propTypes = {
};

export default Computer;
